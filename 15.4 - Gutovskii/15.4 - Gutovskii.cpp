// 15.4 - Gutovskii.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "iostream"


void printNumbers(int n, bool even = true)
{
	for (int i = 0; i <= n; i++)
	{
		bool isEven = i % 2 == 0;
		if (isEven && even)
			std::cout << i << "\n";
		else if (!isEven && !even)
			std::cout << i << "\n";
	}
}

int main()
{
	
	printNumbers(24, true);	
	printNumbers(10, false);
	printNumbers(15, false);

	system("pause");

}

